from django.conf.urls import url
from . import views

app_name = 'manageclass'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^list$', views.student_list, name='list'),
    url(r'^(?P<student_number>[0-9]+)/$', views.student_requirements, name='requirement'),
    url(r'^(?P<student_number>[0-9]+)/detail$', views.student_detail, name='detail'),
    #url(r'^(?P<student_number>[0-9]+)/quizzes$', views.student_quizzes, name='quizzes'),
    #url(r'^(?P<student_number>[0-9]+)/tests$', views.student_tests, name='tests'),
    #url(r'^(?P<student_number>[0-9]+)/projects$', views.student_projects, name='projects'),
]
