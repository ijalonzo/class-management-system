from django.db import models

# Create your models here.
class Student(models.Model):
    student_number = models.IntegerField(primary_key=True)
    student_name = models.CharField(max_length=200)

    def __str__(self):
        return str(self.student_number) + " - " + self.student_name

class Requirement(models.Model):
    student = models.ForeignKey(Student)
    REQ_TYPES = (
        ('Q', 'Quiz'),
        ('T', 'Test'),
        ('P', 'Project'),
    )
    requirement_type = models.CharField(max_length=1, choices=REQ_TYPES)
    raw_score = models.IntegerField()
    highest_possible_score = models.IntegerField()

    def __str__(self):
        return self.student.student_name + " " + self.requirement_type + " " + str(self.raw_score) + "/" + str(self.highest_possible_score)
