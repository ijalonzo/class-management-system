from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from .models import *

# Create your views here.
def index(request):
    students = Student.objects.all()
    requirement = Requirement.objects.all()
    context = {
        'students': students,
        'requirement': requirement,
    }
    return render(request, 'manageclass/index.html', context)

def student_list(request):
    students = Student.objects.all()
    requirement = Requirement.objects.all()

    context = {
        'students': students,
        'requirement': requirement,
    }
    return render(request, 'manageclass/list.html', context)

def student_detail(request, student_number):
    student = Student.objects.get(pk=student_number)
    requirement = Requirement.objects.all()
    total_raw_quizzes = 0
    total_hps_quizzes = 0
    total_raw_tests = 0
    total_hps_tests = 0
    total_raw_projects = 0
    total_hps_projects = 0

    for r in requirement:
        if r.student.student_number == student.student_number:
            if r.requirement_type == 'Q':
                total_raw_quizzes += r.raw_score
                total_hps_quizzes += r.highest_possible_score
            elif r.requirement_type == 'T':
                total_raw_tests += r.raw_score
                total_hps_tests += r.highest_possible_score
            elif r.requirement_type == 'P':
                total_raw_projects += r.raw_score
                total_hps_projects += r.highest_possible_score

    if total_hps_quizzes != 0:
        q_ave = 20 * (total_raw_quizzes/total_hps_quizzes)      #Computes the average of quizzes
    else:
        q_ave = 0

    if total_hps_tests != 0:
        t_ave = 30 * (total_raw_tests/total_hps_tests)          #Computes the average of tests
    else:
        t_ave = 0

    if total_hps_projects != 0:
        p_ave = 50 * (total_raw_projects/total_hps_projects)    #Computes the average of projects
    else:
        p_ave = 0

    #Computes for the GWA o student
    gwa = q_ave + t_ave + p_ave

    #Checks the status of the student
    if gwa < 80:
        status = "Warning"
    else:
        status = "Good"

    context = {
        'student': student,
        'requirement': requirement,
        'total_raw_quizzes': total_raw_quizzes,
        'total_hps_quizzes': total_hps_quizzes,
        'total_raw_tests': total_raw_tests,
        'total_hps_tests': total_hps_tests,
        'total_raw_projects': total_raw_projects,
        'total_hps_projects': total_hps_projects,
        'gwa': gwa,
        'status': status,
    }

    return render(request, 'manageclass/details.html', context)

def student_requirements(request, student_number):
    student = get_object_or_404(Student, pk=student_number)
    requirement = Requirement.objects.all()

    context = {
        'student': student,
        'requirement': requirement,
    }

    return render(request, 'manageclass/requirement.html', context)
